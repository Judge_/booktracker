import { defineStore } from 'pinia'
import { ref } from 'vue'

interface Book {
  olid: string
  name: string
  author: string
  genre: string
  year: number
}

export const useBookStore = defineStore('BookStore', () => {
  const books = ref<Book[]>([])

  async function fill() {
    books.value = (await import('@/sample-data/books.json')).default
  }

  return { books, fill }
})
